FROM openjdk:8-jre-alpine

VOLUME /tmp

ADD build/libs/config-server-0.0.1-SNAPSHOT.jar app.jar

RUN /bin/sh -c 'touch app.jar'

ENV app_name "Config server"

EXPOSE 8081

ENTRYPOINT exec java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar app.jar
